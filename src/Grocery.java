/* Copyright 2021 digital <didev@dinid.net>
 * Copyright 2021 Bea Horn
 * Copyright 2021 Leon Dinh
 *
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, eitherexpress or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.ChronoField;
import java.time.format.DateTimeFormatter;

public class Grocery implements Comparable<Grocery> {
    // the formatter for displaying dates
    private static DateTimeFormatter dtf = DateTimeFormatter.ofPattern("EEE, d LLL y");

    // the associated name for this grocery item
    private String name;
    // the type of grocery item this is
    private String type;
    // the brand that produced this grocery item
    private String brand;
    // the date when this grocery item will expire
    private LocalDate expiryDate;
    // the date when this grocery item was opened
    private LocalDate openDate;
    // number of days this item is good after being opened
    private int expiryTime;
    // the fridge this grocery item is inside. when the item is opened, the
    // position inside the fridge may change, so this item needs to notify
    // fridge of such an event.
    private Fridge fridge;

    // Constructs a Grocery based on the output produced by Grocery.toString().
    public Grocery(String line) {
        // cheap creature's deserialization, except this uses regexes and is
        // probably way overcomplicated. I am very sorry and apologize for my use
        // of regexes but will probably not learn from this experience and still
        // use regexes in the future. Really, only the next line is an absolute
        // horror, so it's not that bad, right?
        Pattern p = Pattern.compile("^(?<name>[^(]+) (\\((?<type>[^)]+)\\) )?von '(?<brand>.*)'" + "(, (vor \\d+ Tagen abgelaufen|noch \\d+ Tage haltbar|läuft heute ab).)?" + " \\((?<expiryDate>\\w+, \\d+ \\w+ \\d+)" + "(, geöffnet (?<openDate>\\w+, \\d+ \\w+ \\d+))?" + "(\\. (?<expiryTime>\\d+) Tage nach öffnen haltbar)?\\)$");
        Matcher m = p.matcher(line);
        m.find();
        this.name = m.group("name");
        this.type = m.group("type");
        this.brand = m.group("brand");
        this.expiryDate = LocalDate.now();
        this.expiryDate = LocalDate.parse(m.group("expiryDate"), DateTimeFormatter.ofPattern("EEE, d LLL y"));
        if (m.group("expiryTime") == null) {
            this.expiryTime = -1;
        } else {
            this.expiryTime = Integer.parseInt(m.group("expiryTime"));
        }
        if (m.group("openDate") != null) {
            this.openDate = LocalDate.parse(m.group("openDate"), DateTimeFormatter.ofPattern("EEE, d LLL y"));
        }
    }

    public Grocery(String name, String type, String brand, LocalDate expiryDate) {
        this(name, type, brand, expiryDate, null,  -1);
    }
    public Grocery(String name, String type, String brand, LocalDate expiryDate, int expiryTime) {
        this(name, type, brand, expiryDate, null, expiryTime);
    }
    public Grocery(String name, String type, String brand, LocalDate expiryDate, LocalDate openDate) {
        this(name, type, brand, expiryDate, openDate, -1);
    }
    public Grocery(String name, String type, String brand, LocalDate expiryDate, LocalDate openDate, int expiryTime) {
        this.name = name;
        this.type = type;
        this.brand = brand;
        this.expiryDate = expiryDate;
        this.openDate = openDate;
        this.expiryTime = expiryTime;
    }

    public String getName() {
        return name;
    }
    public LocalDate getOpenDate() {
        return openDate;
    }

    // set a new openDate. can be used to overwrite the existing openDate
    public void setOpenDate(LocalDate date) {
        this.openDate = date;
        this.fridge.updatePosition(this);
    }

    // open this grocery item now. Returns true on success and false if the grocery
    // is already open.
    public boolean open() {
        if (this.openDate == null) {
            this.openDate = LocalDate.now();
            this.fridge.updatePosition(this);
            return true;
        } else {
            return false;
        }
    }

    // overwrites the fridge this grocery item is currently inside. this will
    // try to remove the grocery item from a previous fridge.
    protected void setFridge(Fridge f) {
        if (this.fridge != null) {
            this.fridge.removeGrocery(this);
        }
        this.fridge = f;
    }

    public Fridge getFridge() {
        return this.fridge;
    }

    public int getExpiryTime() {
        return expiryTime;
    }
    public LocalDate getExpiryDate() {
        return expiryDate;
    }
    public String getType() {
        return type;
    }
    public String getBrand() {
        return brand;
    }

    // Returns true if the grocery has expired, this happens when the expiry
    // date has passed or when expiryTime days since openDate have passed.
    public boolean isExpired() {
        // if expiry date has passed
        if (LocalDate.now().compareTo(this.expiryDate) > 0) {
            return true;
        }
        // check wether expiry time has passed, we need an open date and
        // expiryTime for this
        if (this.openDate != null && this.expiryTime >= 0) {
            LocalDate expiryTimeDate = LocalDate.from(this.openDate);
            expiryTimeDate = expiryTimeDate.plusDays(this.expiryTime);
            return LocalDate.now().compareTo(expiryTimeDate) > 0;
        }
        return false;
    }

    // Returns true only if this grocery item has been open.
    public boolean isOpen() {
        if (this.openDate == null) {
            return false;
        }
        // testing only for the existance of an openDate should be enough if
        // the software is used as intended
        return LocalDate.now().compareTo(this.openDate) >= 0;
    }

    // calculates tne number of days for which this grocery item is still
    // fresh. if this grocery item already went bad, calculate the number of
    // days for which this grocery item has been expired. if the grocery item
    // expires today, this method returns 0.
    public long calculateGoodTime() {
        // calculate number of days to expiry date
        long nowDays = LocalDate.now().getLong(ChronoField.EPOCH_DAY);
        long expiryDateDays = this.expiryDate.getLong(ChronoField.EPOCH_DAY);
        long daysToExpiryDate = expiryDateDays - nowDays;
        if (this.isOpen() && this.expiryTime != -1) {
            // calculate number of days until expiry time number of days have
            // passed since open date
            LocalDate maxOpenDaysDate = LocalDate.from(this.openDate).plusDays(this.expiryTime);
            long maxOpenDaysDateDays = maxOpenDaysDate.getLong(ChronoField.EPOCH_DAY);
            long daysToMaxOpenDaysDate = maxOpenDaysDateDays - nowDays;
            if (daysToExpiryDate < daysToMaxOpenDaysDate) {
                return daysToMaxOpenDaysDate;
            }
        }
        return daysToExpiryDate;
    }

    public String toString() {
        String text = this.name + " (" + this.type + ") von '" + this.brand + "'";
        long good = this.calculateGoodTime();
        if (good < 0) {
            text += ", vor " + -good + " Tagen abgelaufen.";
        } else if (good > 0) {
            text += ", noch " + good + " Tage haltbar.";
        } else {
            text += ", läuft heute ab.";
        }
        text += " (" + this.dtf.format(this.expiryDate);
        if (this.isOpen()) {
            text += ", geöffnet " + this.dtf.format(this.openDate);
        }
        if (this.expiryTime >= 0) {
            text += ". " + this.expiryTime + " Tage nach öffnen haltbar";
        }
        text += ")";
        return text;
       //return "Name:\t" + name + "\n Open LocalDate:\t" + openDate + "\n Expiry LocalDate:\t" + expiryDate;
    }

    public int compareTo(Grocery other) {
        return (int) (other.calculateGoodTime() - this.calculateGoodTime());
        //return this.expiryDate.compareTo(other.expiryDate);
    }
}
