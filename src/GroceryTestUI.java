/* Copyright 2021 digital <didev@dinid.net>
 * Copyright 2021 Bea Horn
 * Copyright 2021 Leon Dinh
 *
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, eitherexpress or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

import java.util.ArrayList;
import java.util.Calendar;
import java.io.*;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.format.DateTimeFormatter;

class GroceryTestUI {
    public static void main(String[] args) {
        LocalDate past = LocalDate.now().minusDays(6);
        Grocery grocery = new Grocery("Gurke", "Gemüse", "Einfach Gurkig", past, 35);
        LocalDate past2 = LocalDate.now().plusDays(4);
        Grocery grocery2 = new Grocery("Paprika", "Gemüse", "Einfach Paprig", past2, 35);
        System.out.println(grocery);
        System.out.println("Lebensmittel ist abgelaufen. abgelaufen: " + grocery.isExpired());
        System.out.println("Lebensmittel ist nicht offen. offen: " + grocery.isOpen());
        System.out.println(grocery2);
        System.out.println("gurke > paprika: " + (grocery.compareTo(grocery2) > 0));

        Grocery g = new Grocery("Banane (Obst) von 'Banane24', noch 3 Tage haltbar. (Wed, 9 Jun 2021. 35 Tage nach öffnen haltbar)");
        System.out.println("deserialized banana: " + g);
        try {
            LocalDate l = LocalDate.parse("Wed, 9 Jun 2021", DateTimeFormatter.ofPattern("EEE, d LLL y"));
            System.out.println("date of banana is: " + l);
        } catch (Exception e) {
            System.out.println(e);
        }

        System.out.println("\nTest of serialization and deserialization");
        for (Grocery g1: TestData.makeGroceryList()) {
            Grocery g2 = new Grocery(g1.toString());
            if (!g2.toString().equals(g1.toString())) {
                System.out.println("vv mismatch vv");
            }
            System.out.println(g1);
            System.out.println(g2);
        }
    }
}
