/* Copyright 2021 digital <didev@dinid.net>
 * Copyright 2021 Bea Horn
 * Copyright 2021 Leon Dinh
 *
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, eitherexpress or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

import java.util.Scanner;
import java.util.ArrayList;
import java.time.LocalDate;

class CliUI {
    private static final Scanner scanner = new Scanner(System.in);
    private Fridge fridge;
    private boolean hasUnsavedChanges = false;

    public static void main(String[] args) {
        new CliUI().run(args);
    }

    private void run(String[] args) {
        System.out.println("Wilkommen bei der Kühlschrankverwaltung");
        System.out.println("Lizensiert unter EUPL 1.2 (https://joinup.ec.europa.eu/software/page/eupl)");
        if (args.length == 0) {
            System.out.println("\n-Hinweis:");
            System.out.println("Keine Datei zum speichern angegeben, Kühlschrank ist leer.\n\n-Hilfe:");
            System.out.println("Sie können eine nicht existierende, leere oder gespeicherte Datei angeben.");
            System.out.println("Um eine Datei anzugeben, führen sie das Programm so aus:");
            System.out.println(" java CliUI <dateipfad>\n");
            System.out.println("Um eine Datei mit aktuellen Testdaten zu bekommen, führen sie aus:");
            System.out.println(" java TestData <dateipfad>\n");
            System.out.println("Die so gespeicherten Testdaten können sie dann mit diesem Befehl laden:");
            System.out.println(" java CliUI testdaten.txt\n");
            System.out.println("Alternativ können sie Dateien und Testdaten vom 'Speichern und Laden' Menü laden.");
            CliUI.askWait();
            this.fridge = new Fridge();
        } else {
            this.fridge = new Fridge(args[0]);
        }
        this.doMainMenu();
        System.out.println("\nVielen Dank für die Benutzung der Kühlschrankverwaltung.");
    }

    private void doMainMenu() {
        String[] choices = {
            "Lebensmittel anzeigen",
            "Lebensmittel suchen",
            "Lebensmittel hinzufügen",
            "Lebensmittel entfernen",
            "Lebensmittel öffnen",
            "Speichern und Laden",
            "Programm beenden",
        };
        while (true) {
            System.out.println("\n-Hauptmenü");
            System.out.println("Was wollen sie tun?");
            int choice = this.askChooseOne(choices);
            System.out.println();
            switch (choice) {
                case 0:
                    this.doMenuShowGroceries();
                    break;
                case 1:
                    this.doMenuSearchGroceries();
                    break;
                case 2:
                    this.doMenuAddGrocery();
                    break;
                case 3:
                    this.doMenuRemoveGrocery();
                    break;
                case 4:
                    this.doMenuOpenGrocery();
                    break;
                case 5:
                    this.doMenuSaveLoad();
                    break;
                default:
                    if (this.doMenuExit()) {
                        return;
                    }
            }
        }
    }

    private void doMenuSaveLoad() {
        String[] choices = {
            "Speichern",
            "Speichern unter",
            "Laden",
            "Kühlschrank leeren und dann laden",
            "Testdaten Laden",
        };
        System.out.println("-Speichern und Laden");
        System.out.println("aktueller Speicherpfad: " + this.fridge.getSavePath());
        if (this.hasUnsavedChanges) {
            System.out.println("Sie haben ungespeicherte Änderungen.");
        }
        switch (this.askChooseOneOrBack(choices)) {
            case 0:
                if (!this.fridge.save()) {
                    System.out.println("Speichern fehlgeschlagen.");
                    this.askWait();
                }
                break;
            case 1:
                System.out.print("Neuer Speicherpfad: ");
                this.fridge.setSavePath(this.inpWords());
                this.fridge.save();
                break;
            // ohh look, I'm using the stupid switch case mechanic that mostly
            // causes problems and is very confusing!
            case 3:
                if (this.hasUnsavedChanges) {
                    System.out.println("Sie haben ungespeicherte Änderungen, wollen sie diese wirklich löschen?");
                    if (!this.inpBool(false)) {
                        break;
                    }
                    this.fridge.removeAllGroceries();
                }
            case 2:
                System.out.print("Dateipfad: ");
                this.fridge.load(this.inpWords());
                break;
            case 4:
                System.out.print("Testdaten dem Kühlschrank hinzufügen?");
                if (this.inpBool()) {
                    for (Grocery g: TestData.makeGroceryList()) {
                        this.fridge.addGrocery(g);
                    }
                    this.hasUnsavedChanges = true;
                }
                break;
        }
    }

    private boolean doMenuExit() {
        String[] choices = {
            "Speichern und beenden",
            "Speichern unter und beenden",
            "Ohne speichern beenden",
        };
        System.out.println("-Programm beenden");
        System.out.println("aktueller Speicherpfad: " + this.fridge.getSavePath());
        if (this.hasUnsavedChanges) {
            System.out.println("Sie haben ungespeicherte Änderungen.");
        } else {
            System.out.println("Sie haben keine ungespeicherte Änderungen.");
        }
        switch (this.askChooseOneOrBack(choices)) {
            case 0:
                if (this.fridge.save()) {
                    return true;
                } else {
                    System.out.println("Speichern fehlgeschlagen.");
                    this.askWait();
                    return false;
                }
            case 1:
                System.out.print("Neuer Speicherpfad: ");
                this.fridge.setSavePath(this.inpWords());
                return this.fridge.save();
            case 2:
                if (this.hasUnsavedChanges) {
                    System.out.print("Sind sie sich sicher, dass sie ihre ungespeicherten Änderungen verwerfen wollen?");
                    if (this.inpBool(false)) {
                        return true;
                    }
                } else {
                    return true;
                }
                return false;
            default:
                return false;

        }
    }


    private void doMenuRemoveGrocery() {
        String[] choices = {
            "Einzelnes Lebensmittel entfernen",
            "Einzelnes angebrochenes Lebensmittel entfernen",
            "Einzelnes geschlossenes Lebensmittel entfernen",
            "Einzelnes frisches Lebensmittel entfernen",
            "Einzelnes abgelaufenes Lebensmittel entfernen",
            "Alle abgelaufenen Lebensmittel entfernen",
            "Alle Lebensmittel entfernen",
        };
        while (true) {
            System.out.println("-Lebensmittel entfernen");
            switch (this.askChooseOneOrBack(choices)) {
                case 0:
                    if (this.doMenuRemoveGrocery(this.fridge.getGroceries())) {
                        return;
                    }
                    break;
                case 1:
                    if (this.doMenuRemoveGrocery(this.fridge.searchOpenGroceries())) {
                        return;
                    }
                    break;
                case 2:
                    if (this.doMenuRemoveGrocery(this.fridge.searchClosedGroceries())) {
                        return;
                    }
                    break;
                case 3:
                    if (this.doMenuRemoveGrocery(this.fridge.searchFreshGroceries())) {
                        return;
                    }
                    break;
                case 4:
                    if (this.doMenuRemoveGrocery(this.fridge.searchExpiredGroceries())) {
                        return;
                    }
                    break;
                case 5:
                    this.doMenuRemoveExpired();
                    return;
                case 6:
                    this.doMenuRemoveAll();
                    return;
                default:
                    return;
            }
        }
    }

    private boolean doMenuRemoveGrocery(ArrayList<Grocery>choices ) {
        int inp = this.askChooseOneOrBack(choices);
        if (inp >= this.fridge.getGroceries().size()) {
            return true;
        }
        Grocery del = choices.get(inp);
        this.fridge.removeGrocery(del);
        this.hasUnsavedChanges = true;
        System.out.println("entferntes Lebensmittel: " + del);
        this.askWait();
        return false;
    }

    private void doMenuShowGroceries() {
        String[] choices = {
            "Alle Lebensmittel anzeigen",
            "Alle angebrochenen Lebensmittel anzeigen",
            "Alle geschlossenen Lebensmittel anzeigen",
            "Alle frischen Lebensmittel anzeigen",
            "Alle abgelaufenen Lebensmittel anzeigen",
        };
        System.out.println("-Lebensmittel anzeigen");
        switch (this.askChooseOneOrBack(choices)) {
            case 0:
                this.listAllGroceries();
                break;
            case 1:
                this.listOpenGroceries();
                break;
            case 2:
                this.listClosedGroceries();
                break;
            case 3:
                this.listFreshGroceries();
                break;
            case 4:
                this.listExpiredGroceries();
                break;
            default:
                return;
        }
    }

    private void doMenuSearchGroceries() {
        String[] choices = {
            "Nach Name suchen",
            "Nach Kategorie suchen",
            "Nach Marke suchen",
        };
        System.out.println("-Lebensmittel suchen");
        int criteria = this.askChooseOneOrBack(choices);
        if (criteria == 3) {
            return;
        }
        System.out.print("Suchanfrage: ");
        String query = this.inpWords();
        ArrayList<Grocery> results = null;
        switch (criteria) {
            case 0:
                results = fridge.searchGroceriesWithName(query);
                break;
            case 1:
                results = fridge.searchGroceriesOfType(query);
                break;
            case 2:
                results = fridge.searchGroceriesFromBrand(query);
                break;
        }
        for (Grocery g : results) {
            System.out.println("|-" + g);
        }
        this.askWait();
    }

    private void doMenuOpenGrocery() {
        System.out.println("-Lebensmittel öffnen");
        ArrayList<Grocery> groceries = this.fridge.searchClosedGroceries();
        if (groceries.size() == 0) {
            System.out.println("Kann keine Lebensmittel öffnen, da der Kühlschrank keine geschlossenen Lebensmittel enthält.");
            this.askWait();
            return;
        }
        int choice = this.askChooseOneOrBack(groceries);
        if (choice < groceries.size()) {
            groceries.get(choice).open();
            System.out.println("-Diese Lebensmittel wurden geöffnet:\n|-" + groceries.get(choice));
            this.askWait();
        }
    }

    private void doMenuRemoveAll() {
        if (this.fridge.searchExpiredGroceries().size() == 0) {
            System.out.println("Keine Abgelaufenen Lebensmittel im Kühlschrank.");
            this.askWait();
            return;
        }
        System.out.print("Sind sie sich sicher, dass sie alle Lebensmittel aus dem Kühlschrank entfernen wollenf?");
        boolean answer = this.inpBool(false);
        if (answer) {
            System.out.println("-Diese Lebensmittel wurden entfernt:");
            for (Grocery g : this.fridge.removeAllGroceries()) {
                System.out.println("|-" + g);
            }
            this.hasUnsavedChanges = true;
        } else {
            System.out.println("Keine Lebensmittel entfernt.");
        }
        this.askWait();
    }

    private void doMenuRemoveExpired() {
        if (this.fridge.searchExpiredGroceries().size() == 0) {
            System.out.println("Keine Abgelaufenen Lebensmittel im Kühlschrank.");
            this.askWait();
            return;
        }
        System.out.print("Sind sie sich sicher, dass sie alle abgelaufenen Lebensmittel aus dem Kühlschrank entfernen wollen?");
        boolean answer = this.inpBool(false);
        if (answer) {
            System.out.println("-Diese Lebensmittel wurden entfernt:");
            for (Grocery g : this.fridge.removeExpiredGroceries()) {
                System.out.println("|-" + g);
            }
            this.hasUnsavedChanges = true;
        } else {
            System.out.println("Keine Lebensmittel entfernt.");
        }
        this.askWait();
    }

    // prompts the user for the details of a new grocery item, then gives the
    // option to add it to the fridge
    private void doMenuAddGrocery() {
        String name;
        String brand;
        String type;
        LocalDate expiry;
        int expiryTime;
        Grocery grocery;
        LocalDate open = null;
        System.out.println("-Lebensmittel hinzufügen");
        System.out.println("Bitte geben sie die Informationen über das Lebensmittel ein:");
        System.out.print("Name: ");
        name = this.inpWords();
        System.out.print("Marke: ");
        brand = this.inpWords();
        System.out.print("Kategorie: ");
        type = this.inpWords();
        System.out.print("Haltbarkeitsdatum (dd.mm.yyyy): ");
        expiry = this.inpDate(false);
        System.out.print("Tage Haltbar nach öffnen (Zahl oder leer): ");
        expiryTime = this.inpIntOrEmpty();
        System.out.print("Geöffnet");
        if (this.inpBool(false)) {
            System.out.print("Geöffnet am (dd.mm.yyyy oder leer lassen für heute): ");
            open = this.inpDate(true);
        }
        if (expiryTime < 0) {
            grocery = new Grocery(name, brand, type, expiry, open);
        } else {
            grocery = new Grocery(name, brand, type, expiry, open, expiryTime);
        }
        System.out.println("Erstelltes Lebensmittel: " + grocery);
        System.out.print("Lebensmittel hinzufügen?");
        if (this.inpBool(true)) {
            this.fridge.addGrocery(grocery);
            this.hasUnsavedChanges = true;
            System.out.println("Lebensmittel hinzugegfügt");
        } else {
            System.out.println("Lebensmittel nicht hinzugefügt");
        }
        this.askWait();
    }

    private void listAllGroceries() {
        System.out.println("-Alle Lebensmittel im Kühlschrank:");
        for (Grocery g : this.fridge.getGroceries()) {
            System.out.println("|-" + g);
        }
        this.askWait();
    }

    private void listOpenGroceries() {
        System.out.println("-Geöffnete Lebensmittel im Kühlschrank:");
        for (Grocery g : this.fridge.searchOpenGroceries()) {
            System.out.println("|-" + g);
        }
        this.askWait();
    }

    private void listExpiredGroceries() {
        System.out.println("-Abgelaufene Lebensmittel im Kühlschrank:");
        for (Grocery g : this.fridge.searchExpiredGroceries()) {
            System.out.println("|-" + g);
        }
        this.askWait();
    }

    private void listClosedGroceries() {
        System.out.println("-Geschlossene Lebensmittel im Kühlschrank:");
        for (Grocery g : this.fridge.searchClosedGroceries()) {
            System.out.println("|-" + g);
        }
        this.askWait();
    }

    private void listFreshGroceries() {
        System.out.println("-Frische Lebensmittel im Kühlschrank:");
        for (Grocery g : this.fridge.searchFreshGroceries()) {
            System.out.println("|-" + g);
        }
        this.askWait();
    }

    // similar to `askChooseOneOrBack` but works chooses from a list of grocery
    // items instead.
    private int askChooseOneOrBack(ArrayList<Grocery> choices) {
        String[] strings = new String[choices.size() + 1];
        for (int i = 0; i < choices.size(); i++) {
            strings[i] = choices.get(i).toString();
        }
        strings[choices.size()] = "Zurück";
        return askChooseOne(strings);
    }

    // similar to `askChooseOne` but also offers the choice of going back. if
    // the back option is chosen, the size of the given array is returned,
    // which is the lowest integer that is not a valid index for the array.
    private int askChooseOneOrBack(String[] choices) {
        String[] choices2 = new String[choices.length + 1];
        for (int i = 0; i < choices.length; i++) {
            choices2[i] = choices[i];
        }
        choices2[choices.length] = "Zurück";
        return askChooseOne(choices2);
    }

    // prompts the user to pick one of the given option.
    // the given options are numbered and displayed, then the user can enter
    // the number they want to pick. this function propts until a valid index
    // into the given array is entered.
    private int askChooseOne(String[] choices) {
        for (int i = 0; i < choices.length; i++) {
            System.out.println((i + 1) + ": " + choices[i]);
        }
        System.out.print("Bitte wählen sie eine Option aus: ");
        while (true) {
            try {
                int inp = this.scanner.nextInt() - 1;
                if (inp < choices.length) {
                    this.scanner.nextLine();
                    return inp;
                }
            } catch (java.util.InputMismatchException e) {
                this.scanner.nextLine();
            }
            System.out.print("Unklare Eingabe. Bitte wählen sie eine Option aus: ");
        }
    }

    // prompts the user to press enter. this function is intended to be used to
    // direct the attention of the user towards the output done previous to
    // calling this function.
    private static void askWait() {
        System.out.print("[Enter drücken]");
        CliUI.scanner.nextLine();
    }

    // prompts the user for a Date in the form of dd.mm.yyyy. only returns once
    // the user has entered a well formatted date.
    private LocalDate inpDate(boolean empty) {
        String inp;
        LocalDate date;
        while (true) {
            inp = this.scanner.nextLine();
            if (empty && inp.equals("")) {
                return LocalDate.now();
            }
            try {
                String[] inps = inp.split("\\.");
                date = LocalDate.of(
                    Integer.parseInt(inps[2]),
                    Integer.parseInt(inps[1]),
                    Integer.parseInt(inps[0])
                );
                return date;
            } catch (java.lang.NumberFormatException | java.lang.ArrayIndexOutOfBoundsException e) {
                System.out.print("Unklare Eingabe. Bitte geben sie ein Datum ein.\n> ");
            }
        }
    }

    // prompts the user for an Integer. if no or negative input is given, a
    // negative number is returned to indicate no input.
    private int inpIntOrEmpty() {
        int i;
        String inp;
        while (true) {
            inp = this.scanner.nextLine();
            if (inp.equals("")) {
                return -1;
            }
            try {
                i = Integer.parseInt(inp);
                return i;
            } catch (java.lang.NumberFormatException e) {
                System.out.print("Unklare Eingabe. Bitte geben sie eine ganze Zahl oder nichts ein.\n> ");
            }
        }
    }

    // prompts the user for single line text, ie multiple words. prompts again
    // on empty input (user just presses enter).
    private String inpWords() {
        while (true) {
            String s = this.scanner.nextLine();
            if (!s.equalsIgnoreCase("")) {
                return s;
            }
            System.out.print("Bitte geben sie Text ein:\n> ");
        }
    }

    // prompts the user for a yes / no question and only returns once a correct
    // input was received.
    private boolean inpBool() {
        System.out.print(" [yn] ");
        while (true) {
            String s = this.scanner.nextLine();
            if (s.equalsIgnoreCase("y")) {
                return true;
            } else if (s.equalsIgnoreCase("n")) {
                return false;
            }
            System.out.print("Unklare Eingabe, Bitte antworten sie mit Yes oder No");
        }
    }

    // prompts the user for a yes / no question and only returns once a correct
    // input was received. if the user submits empty input (just presses
    // enter), the given `defa` (for default) value is returned. the visual
    // prompt respects this variable.
    private boolean inpBool(boolean defa) {
        while (true) {
            if (defa) {
                System.out.print(" [Yn] ");
            } else {
                System.out.print(" [yN] ");
            }
            String s = this.scanner.nextLine();
            if (s.equalsIgnoreCase("y") || (defa && s.equalsIgnoreCase(""))) {
                return true;
            } else if (s.equalsIgnoreCase("n") || (!defa && s.equalsIgnoreCase(""))) {
                return false;
            }
            System.out.print("Unklare Eingabe, Bitte antworten sie mit Yes oder No");
        }
    }
}
