/* Copyright 2021 digital <didev@dinid.net>
 * Copyright 2021 Bea Horn
 * Copyright 2021 Leon Dinh
 *
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, eitherexpress or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

import java.util.ArrayList;
import java.util.Collections;
import java.io.*;
import java.nio.file.*;
import java.nio.charset.StandardCharsets;

class Fridge {
    // this list holds all groceries in the fridge and is sorted with long ago
    // expired items first and still good items last.
    private ArrayList<Grocery> groceries = new ArrayList<Grocery>();
    // path to the file to load and save from
    private Path savePath;

    public Fridge() {}

    // creates a new Fridge by loading groceries from a file. the given path
    // becomes the savePath.
    public Fridge(String path) {
        this.savePath = Paths.get(path);
        this.load(path);
    }

    public void load(String pathString) {
        Path path = Paths.get(pathString);
        try {
            BufferedReader br = Files.newBufferedReader(path, StandardCharsets.UTF_8);
            String line;
            while ((line = br.readLine()) != null) {
                this.addGrocery(new Grocery(line));
            }
            br.close();
        } catch (IOException e) {
            System.out.println("Lebensmittel konnte nicht geladen werden, da ein Fehler aufgetreten ist: " + e);
        }
    }

    // create a new fridge based on an existing list of groceries.
    public Fridge(ArrayList<Grocery> groceries) {
        for (Grocery g: groceries) {
            g.setFridge(this);
        }
        Collections.sort(groceries);
        this.groceries = groceries;
    }

    // adds a grocery to the fridge while keeping the groceries in the fridge
    // sorted by "good time". This metric is the number of days until a grocery
    // will expire, or the number of days since it has expired.
    //
    // this function will insert the grocery inside the given range (inclusive
    // min, inclusive max) by recursively checking whether the new grocery item
    // should be placed next to, bevore or after the grocery item in the middle
    // of the range. the range is inclusive on both ends because for n - 1
    // positions there are n positions to insert a new item (eg before each
    // item, so n positions, plus after the last item, the n+1th position
    public void addGrocery(Grocery grocery, int min, int max) {
        if (min == max) {
            // it is impossible to subdivide the range further, therfore the
            // new grocery item is inserted here.
            this.groceries.add(min, grocery);
            return;
        }
        int middle = min + (max - min) / 2;
        int cmp = grocery.compareTo(this.groceries.get(middle));
        if (cmp > 0) {
            // new grocery expires after middle grocery
            // this if statement ruled out the middle position, therefore it is
            // save to add one to the new min value, decreasing the search
            // space.
            this.addGrocery(grocery, middle + 1, max);
        } else if (cmp < 0) {
            // new grocery expireds before middle grocery
            this.addGrocery(grocery, min, middle);
        } else {
            // middle grocery expires at the same time new grocery does,
            // therefore middle is the correct place for new grocery.
            this.groceries.add(middle, grocery);
            return;
        }
    }


    // Adds a single grocery item to the fridge. The list is sorted by the
    // number of days for how a grocery is still good, or if the grocery is
    // already expired, the number of days since it expired.
    public void addGrocery(Grocery grocery) {
        grocery.setFridge(this);
        if (this.groceries.size() == 0) {
            this.groceries.add(grocery);
        } else {
            this.addGrocery(grocery, 0, this.groceries.size());
        }
        return;
    }

    // remove the given grocery item from this fridge. Returns true if an item
    // was removed.
    public boolean removeGrocery(Grocery g) {
        if (this.groceries.remove(g)) {
            g.setFridge(null);
            return true;
        }
        return false;
    }

    // remove all groceries from the fridge. returns a list
    // of removed groceries.
    public ArrayList<Grocery> removeAllGroceries() {
        ArrayList<Grocery> groceries = this.groceries;
        this.groceries = new ArrayList<Grocery>();
        for (Grocery g: groceries) {
            g.setFridge(null);
        }
        return groceries;
    }

    // clean out the fridge by removing all expired groceries. returns a list
    // of removed groceries.
    public ArrayList<Grocery> removeExpiredGroceries() {
        Grocery g;
        int i = 0;
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        while (i < this.groceries.size()) {
            g = this.groceries.get(i);
            if (g.isExpired()) {
                this.removeGrocery(g);
                results.add(g);
            } else {
                i += 1;
            }
        }
        return results;
    }

    // returns a list of grocery items that all have the given type
    public ArrayList<Grocery> searchGroceriesOfType(String type) {
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        for (Grocery g: this.groceries) {
            if (g.getType().equalsIgnoreCase(type)) {
                results.add(g);
            }
        }
        return results;
    }

    // returns a list of grocery items that have the given name
    public ArrayList<Grocery> searchGroceriesWithName(String name) {
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        for (Grocery g: this.groceries) {
            if (g.getName().equalsIgnoreCase(name)) {
                results.add(g);
            }
        }
        return results;
    }

    // returns a list of grocery items from the given brand
    public ArrayList<Grocery> searchGroceriesFromBrand(String name) {
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        for (Grocery g: this.groceries) {
            if (g.getBrand().equalsIgnoreCase(name)) {
                results.add(g);
            }
        }
        return results;
    }

    // Create a new ArrayList containing all expired groceries.
    public ArrayList<Grocery> searchExpiredGroceries() {
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        for (Grocery g: this.groceries) {
            if (g.isExpired()) {
                results.add(g);
            }
        }
        return results;
    }

    // returns a list of all groceries inside the fridge that are not expired
    public ArrayList<Grocery> searchFreshGroceries() {
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        for (Grocery g: this.groceries) {
            if (!g.isExpired()) {
                results.add(g);
            }
        }
        return results;
    }

    // returns a list of all groceries inside the fridge that are closed.
    public ArrayList<Grocery> searchClosedGroceries() {
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        for (Grocery g: this.groceries) {
            if (!g.isOpen()) {
                results.add(g);
            }
        }
        return results;
    }

    // returns a list of all groceries inside the fridge that are open.
    public ArrayList<Grocery> searchOpenGroceries() {
        ArrayList<Grocery> results = new ArrayList<Grocery>();
        for (Grocery g: this.groceries) {
            if (g.isOpen()) {
                results.add(g);
            }
        }
        return results;
    }

    public ArrayList<Grocery> getGroceries() {
        return this.groceries;
    }

    public void setSavePath(String savePath) {
        this.savePath = Paths.get(savePath);
    }

    public Path getSavePath() {
        return this.savePath;
    }

    // this function repositions the given grocery item in the internal list.
    // this is called when a grocery items gets opened or changes its the open
    // date.
    protected void updatePosition(Grocery grocery) {
        this.removeGrocery(grocery);
        this.addGrocery(grocery);
    }

    // returns the number of grocery items in the fridge.
    public int countGroceries() {
        return this.groceries.size();
    }

    // saves the contents of the fridge to the location it was loaded from
    public boolean save() {
        if (this.savePath == null) {
            System.out.println("Kein Speicherpfad angegeben.");
            return false;
        }
        try {
            BufferedWriter writer = Files.newBufferedWriter(this.savePath, StandardCharsets.UTF_8);
            for (Grocery g: this.groceries) {
                writer.write(g.toString() + "\n");
            }
            writer.close();
            return true;
        } catch (IOException e) {
            System.out.println("Kühlschrank konnte nicht gespeichert werden, da ein Fehler aufgetreten ist:");
            e.printStackTrace();
        }
        return false;
    }

    public String toString() {
        if (this.countGroceries() == 0) {
            return "Kühlschrank ist leer.";
        }

        String text = "==Kühlschrank enthält " + this.countGroceries() + " Lebensmittel:";
        for (Grocery g: this.groceries) {
            text += "\n  " + g;
        }
        return text;
    }

    // pretty print the contents of the fridge, first all expired items, then
    // all open items and then items that fit neither of the first two
    // categories. the same item may be listed as expired and open.
    public String toStringPretty() {
        if (this.countGroceries() == 0) {
            return "Kühlschrank ist leer.";
        }

        ArrayList<Grocery> open = new ArrayList<Grocery>();
        ArrayList<Grocery> boring = new ArrayList<Grocery>();
        ArrayList<Grocery> expired = new ArrayList<Grocery>();

        for (Grocery g: this.groceries) {
            if (g.isExpired()) {
                expired.add(g);
            }
            if (g.isOpen()) {
                open.add(g);
            }
            if (!g.isOpen() && !g.isExpired()) {
                boring.add(g);
            }
        }

        String text = "==Kühlschrank enthält " + this.countGroceries() + " Lebensmittel";
            if (expired.size() != 0) {
            text += "\n--Abgelaufene Lebensmittel (" + expired.size() + "):";
            for (Grocery g: expired) {
                text += "\n    " + g;
            }
        }
        if (open.size() != 0) {
            text += "\n--Offene Lebensmittel (" + open.size() + "):";
            for (Grocery g: open) {
                text += "\n    " + g;
            }
        }
        if (boring.size() != 0) {
            text += "\n--Sonstige Lebensmittel (" + boring.size() + "):";
            for (Grocery g: boring) {
                text += "\n    " + g;
            }
        }
        return text;
    }
}
