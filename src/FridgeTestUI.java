/* Copyright 2021 digital <didev@dinid.net>
 * Copyright 2021 Bea Horn
 * Copyright 2021 Leon Dinh
 *
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, eitherexpress or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

import java.util.ArrayList;

class FridgeTestUI {
    public static void main(String[] args) {
        Fridge f = new Fridge();
        ArrayList<Grocery> groceries = TestData.makeGroceryList();
        for (Grocery g: groceries) {
            f.addGrocery(g);
        }
        ArrayList<Grocery> groceries2 = TestData.makeGroceryList();
        for (Grocery g: groceries2) {
            f.addGrocery(g);
        }
        for (Grocery g: f.getGroceries()) {
            System.out.println(g.calculateGoodTime() + " " + g);
        }

        System.out.println("\n" + f.toStringPretty());


        groceries = f.searchGroceriesOfType("Gemüse");
        System.out.println("\nalles Gemüse:");
        for (Grocery g: groceries) {
            System.out.println("  " + g);
        }

        System.out.println("\njede Paprika:");
        groceries = f.searchGroceriesWithName("Paprika");
        for (Grocery g: groceries) {
            System.out.println("  " + g);
        }

        System.out.println("\nalle Lebensmittel von Rewe:");
        groceries = f.searchGroceriesFromBrand("Rewe");
        for (Grocery g: groceries) {
            System.out.println("  " + g);
        }

        groceries = f.searchOpenGroceries();
        System.out.println("\nalle geöffneten Lebensmittel");
        for(Grocery g: groceries) {
            System.out.println("  " + g);
        }

        groceries = f.searchExpiredGroceries();
        System.out.println("\nalle abgelaufenen Lebensmittel");
        for(Grocery g: groceries) {
            System.out.println("  " + g);
        }

        System.out.println("\n\nentfernte abgelaufene Lebensmittel:");
        for (Grocery g: f.removeExpiredGroceries()) {
            System.out.println("  " + g);
        }
        System.out.println("\nLebensmittel im Kühlschrank:");
        System.out.println(f);
    }
}
