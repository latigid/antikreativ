/* Copyright 2021 digital <didev@dinid.net>
 * Copyright 2021 Bea Horn
 * Copyright 2021 Leon Dinh
 *
 * Licensed under the EUPL, Version 1.2 only (the "Licence");
 * You may not use this work except in compliance with the Licence.
 * You may obtain a copy of the Licence at:
 *
 * https://joinup.ec.europa.eu/software/page/eupl
 *
 * Unless required by applicable law or agreed to inwriting, software
 * distributed under the Licence is distributed on an "AS IS" basis,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, eitherexpress or implied.
 * See the Licence for the specific language governing permissions and
 * limitations under the Licence.
 */

import java.util.ArrayList;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

public class TestData {
    public static void main(String[] args) {
        try {
            FileWriter myWriter = new FileWriter("testdata.txt");
            for (Grocery g: TestData.makeGroceryList()) {
                myWriter.write(g.toString() + "\n");
            }
            myWriter.close();
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        System.out.println("Testdaten wurden in `testdata.txt` geschrieben");
    }

    public static ArrayList<Grocery> makeGroceryList() {
        LocalDate today = LocalDate.now();
        LocalDate past = today.minusDays(7);
        LocalDate future = today.plusDays(7);
        ArrayList<Grocery> g = new ArrayList<Grocery>();
        g.add(new Grocery("Paprika", "Gemüse", "paprika24", future, past));
        g.add(new Grocery("Banane", "Obst", "Demeter", today, past));
        g.add(new Grocery("Tomaten", "Obst", "Natur Lieblinge", past, past));
        g.add(new Grocery("Vollkorn Toatsbrot", "Brot", "Golden Toast", future, today));
        g.add(new Grocery("Blätterteiggebäck", "Gebäck", "Swiss Deleice", today, today));
        g.add(new Grocery("Frischkäse", "Aufstrich", "Philadelphia", future));
        g.add(new Grocery("Apfelsaft", "Getränk", "saftladen", today));
        g.add(new Grocery("Hafermilch", "Getränk", "hafermilch24", past));
        g.add(new Grocery("Muffins", "Nom", "Mom", past, today));
        g.add(new Grocery("Bohnen", "Gemüse", "Garten", past, past, 5));
        g.add(new Grocery("Champignons", "Gemüse", "Rewe", past, future, 5));
        g.add(new Grocery("Paprika", "Gemüse", "Rewe", future, past, 5));
        g.add(new Grocery("Erbsen", "Gemüse", "Erbsen24", future, future, 5));
        return g;
    }
}
